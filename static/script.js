
class LineEditor {
    textarea = $('#file_text');
    constructor() {
        $('#insert_button').click(() => this.insertLine(
            $('#insert_content').val(), $('#insert_line').val()
        ))

        $('#patch_button').click(() => this.patchLine(
            $('#patch_content').val(), $('#patch_line').val()
        ))
        
        $('#delete_button').click(() => this.deleteLine(
            $('#delete_line').val()
        ))
    }

    async read() {
        const result = await fetch('/line');
        const lines = await result.text();
        this.textarea.val(lines);
        return lines;
    }

    async insertLine(content, where) {
        await fetch(`/line/${where}`,
            {
                method: 'POST',
                body: JSON.stringify({content})
            })
        await this.read();
    }

    async patchLine(content, where) {
        await fetch(`/line/${where}`,
            {
                method: 'PATCH',
                body: JSON.stringify({content})
            })
        await this.read();
    }

    async deleteLine(where) {
        await fetch(`/line/${where}`,
            {
                method: 'DELETE',
            })
        await this.read();
    }
}

window.lineEditor = new LineEditor();
