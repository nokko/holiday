# Start with composer:
FROM composer:lts as base-deps
# Labels, metadata:
# Who you should blame for malfunctions:
LABEL authors="nokko"

# Scour the /var/www directory and start anew:
RUN rm -rf /var/www && mkdir -p /var/www/html
WORKDIR /var/www/html

# Copy in our code:
COPY composer.json composer.lock ./
COPY src ./src

# Special sauce for Holiday, we need an empty `db.txt` file in this directory:
RUN touch db.txt && chown www-data db.txt && chgrp www-data db.txt



# Start with the base dependency setup:
FROM base-deps as production-deps

# Install all non-dev packages, quietly:
# Then generate the (optimized) autoload file:
RUN composer install --ignore-platform-reqs --prefer-dist --no-scripts --no-progress --no-interaction --no-dev --no-autoloader \
&& composer dump-autoload --optimize --apcu --no-dev



FROM base-deps as dev-deps
# Install packages, quietly:
# Then generate the (optimized) autoload file:
RUN composer install --ignore-platform-reqs --prefer-dist --no-scripts --no-progress --no-interaction --no-autoloader \
&& composer dump-autoload --optimize --apcu



# Grab php-fpm:
FROM php:8.3-fpm-alpine as base
# Go back to the directory we set up in the earlier stage.
WORKDIR /var/www/html

# Install PHP extensions.

# First, get the useful `install-php-extensions` script:
ADD --chmod=0755 https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

# Use the script:
RUN install-php-extensions pdo_mysql json

# Special sauce for Holiday, we need GNU sed: (as opposed to busybox sed)
RUN apk add --no-cache sed



FROM base as production
# Production release, meaning this is optimized for easy deployment, not easy development:
LABEL release="production"

# Look into configs… later?
#COPY php-fpm/config/php.ini /usr/local/etc/php/

COPY --from=production-deps /var/www/html /var/www/html

# Holiday-specific, run the `collect_endpoints.php` script to generate route definitions:
WORKDIR /var/www/html/src
RUN php -f collect_endpoints.php

# Run php-fpm!
CMD ["php-fpm"]
EXPOSE 9000



FROM base as development
# Development release, meaning this is optimized for easy development, not production deployment:
LABEL release="development"

COPY --from=dev-deps /var/www/html /var/www/html

# Install the xdebug development helper extension: 
RUN install-php-extensions xdebug

# Holiday-specific, run the `collect_endpoints.php` script to generate route definitions:
WORKDIR /var/www/html/src
RUN php -f collect_endpoints.php

# Run php-fpm!
CMD ["php-fpm"]
EXPOSE 9000
