<?php
namespace Nokko\Holiday;
class FileDB {
	static $DB_PATH = '../db.txt';
	static function initialize() {
		FileDB::$DB_PATH = realpath(__DIR__ . '/..' . '/db.txt');

		if (!file_exists(FileDB::$DB_PATH)) {
			file_put_contents(FileDB::$DB_PATH, "\n");
		}
	}

	static function countLines() : int {
		$line_count = 0;
		$handle = fopen(self::$DB_PATH, "r");
		while(!feof($handle)){
			fgets($handle);
			$line_count++;
		}
		fclose($handle);
		return $line_count;
	}

	/**
	 * Add a line to the end of the DB file
	 * @param string $line
	 * @return void
	 */
	static function insert_line(string $line, int $where=-1) {
		if (self::countLines() === 1) {
			$cmd = "echo '$line' > " . realpath(self::$DB_PATH);
			shell_exec($cmd);
			error_log($cmd);
			return;
		}
		if ($where === -1) {
			$line = str_replace('\n', '', $line);
			file_put_contents(FileDB::$DB_PATH, $line . "\n", FILE_APPEND);
		} else {
			$cmd = "sed -i '{$where}i\\\n$line\n' " . realpath(self::$DB_PATH);
			shell_exec($cmd);
			error_log($cmd);
		}
	}

	/**
	 * Replace a line within the DB file.
	 * @param $line
	 * @param $where
	 * @return void
	 */
	static function patch_line($line, $where) {
		$cmd = "sed -i '{$where}c\\\n$line\n' " . realpath(self::$DB_PATH);
		error_log($cmd);
		shell_exec($cmd);
	}

	static function read() {
		return file_get_contents(self::$DB_PATH);
	}

	static function delete_line($where) {
		$cmd = "sed -i '{$where}d' " . realpath(self::$DB_PATH);
		error_log($cmd);
		shell_exec($cmd);
	}
}
