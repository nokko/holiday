<?php

namespace Nokko\Holiday;

use JetBrains\PhpStorm\ArrayShape;

#[\Attribute]
class RouteDef {
	public array $methods;
	public string $pattern;

	public function __construct($methods, $pattern) {
		if (sizeof($methods) < 1) {
			throw new \Error('Cannot use a RouteDef attribute without any methods specified.');
		}

		if (strlen($pattern) < 1) {
			throw new \Error('Cannot use a RouteDef attribute with an empty pattern.');
		}

		$this->methods = $methods;
		$this->pattern = $pattern;
	}

	public function serialize() {
		return [
			'methods' => $this->methods,
			'pattern' => $this->pattern,
		];
	}
}