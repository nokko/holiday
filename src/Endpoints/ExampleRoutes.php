<?php

namespace Nokko\Holiday\Endpoints;

use Nokko\Holiday\FileDB;
use Nokko\Holiday\RouteDef;
use Psr\Http\Message\RequestInterface as Request;
use Slim\Http\Response;

class ExampleRoutes {
	public static function foo() {
		error_log('yike');
	}

	#[RouteDef(['GET'], '/example/')]
	public static function foobar(Request $request, Response $response, $args) {
		return $response->withStatus(400)->withJson([
			'content' => 'example!?!'
		]);
	}
}