<?php

namespace Nokko\Holiday\Endpoints;

use Nokko\Holiday\FileDB;
use Nokko\Holiday\RouteDef;
use Psr\Http\Message\RequestInterface as Request;
use Slim\Http\Response;

class LineRoutes {
	public static function foo() {
		error_log('yike');
	}

	#[RouteDef(['PATCH'], '/line/{line_no}')]
	public static function patchLine(Request $request, Response $response, $args) {
		$line = json_decode($request->getBody()->getContents(), true)['content'];
		if ($line) {
			$line = str_replace('\n', ' ', $line);
			FileDB::patch_line($line, $args['line_no']);
			$db = file(FileDB::$DB_PATH);
			return $response->withJson([
				'content' => $db[$args['line_no'] - 1] ?? '',
			]);
		}
		return $response->withStatus(400)->withJson([
			'content' => ''
		]);
	}

	#[RouteDef(['POST'], '/line/{line_no}')]
	public static function postLine(Request $req, Response $res, $args) {
		$line = json_decode($req->getBody()->getContents(), true)['content'];
		if ($line) {
			FileDB::insert_line($line, $args['line_no']);
		}
		return $res->withJson(['tail' => FileDB::countLines()]);
	}

	#[RouteDef(['GET'], '/line')]
	public static function getLineAll(Request $req, Response $res) {
		return $res->write(FileDB::read());
	}

	#[RouteDef(['GET'], '/line/{line_no}')]
	public static function getLine(Request $request, Response $response, $args) {
		$db = file(FileDB::$DB_PATH);
		// Yucky coercion.
		$line = $args['line_no'] - 1;
		if ((count($db) < $line + 1) || !is_int($line)) {
			return $response->withStatus(404)->withJson([
				'line' => $line + 1,
				'length' => count($db),
			]);
		}

		return $response->withJson([
			'content' => $db[$args['line_no'] - 1] ?? '',
		]);
	}

	#[RouteDef(['DELETE'], '/line/{line_no}')]
	public static function deleteLine(Request $req, Response $res, $args): Response {
		FileDB::delete_line($args['line_no']);
		return $res->withStatus(200);
	}
}