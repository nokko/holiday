<?php

namespace Nokko\Holiday\Endpoints;
use Slim\App;

class Routes {
	static function define(App $app) {
		$route_definitions = require __DIR__ . './../router.raw.php';
		foreach ($route_definitions as $def) {
			$app->map($def['methods'], $def['pattern'], $def['callable']);
		}
		error_log('done defining');
	}
}