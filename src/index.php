<?php

use Slim\Factory\AppFactory;
use Nokko\Holiday\FileDB as FileDB;
use Nokko\Holiday\Pages as Pages;
use Nokko\Holiday\Endpoints as Endpoints;

require '../vendor/autoload.php';

FileDB::initialize();

// Only try the Slim routes if the site can't serve the requested resource.
if (Pages::route()) {
	die();
}

$app = AppFactory::create();

// In production, we should use a cache file to make regex parsing faster:
//$app->getRouteCollector()->setCacheFile(__DIR__ . '/routes.cache.php');

// However, it's best to turn it off during development so that the route cache
// doesn't confuse the developer with stale routes.

Endpoints\Routes::define($app);

$app->addErrorMiddleware(false, true, true);

$app->run();

