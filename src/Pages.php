<?php

namespace Nokko\Holiday;

// Stand-in for existing page routing code; Slim doesn't have to replace it.
class Pages {
	static function route() {
		$path = preg_split('/\//' ,$_SERVER['REQUEST_URI']);
		error_log(json_encode($path));
		if ($path[1] === '' || array_slice($path, -1)[0] === 'index.php') {
			self::serveIndex();
		} elseif ($path[1] === 'static') {
			self::serveStatic($path[2]);
		} elseif ($path[0] === 'secret') {

		} else {
			return false;
		}
		return true;
	}

	static function serveIndex() {
		?>
		<!doctype html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport"
			      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
			<meta http-equiv="X-UA-Compatible" content="ie=edge">
			<link rel="stylesheet" href="/static/style.css">
			<title>Index Page</title>
		</head>
		<body>
		<h1>Line Editor</h1>
		<div class="double">
			<div id="file_area" class="col">
				<label for="file_text"><b>File Contents</b></label>
				<textarea id="file_text" cols="80" rows="60" readonly><?=
					FileDB::read()
					?></textarea>
			</div>
			<div class="col">
				<div>
					<p>
						<b>Insert a line.</b>
					</p>
					<label for='insert_line'>At line:</label>
					<input id='insert_line' type="number" min="1">
					<label for="insert_content">Content:</label>
					<input id='insert_content' type="text">
					<button id="insert_button">Insert!</button>
				</div>
				<div>
					<p>
						<b>Delete a line.</b>
					</p>
					<label for='delete_line'>Line:</label>
					<input id='delete_line' type="number" min="1">
					<button id="delete_button">Delete!</button>
				</div>
				<div>
					<p>
						<b> Change a line.</b>
					</p>
					<label for='patch_line'>Line:</label>
					<input id='patch_line' type="number" min="1">
					<label for="patch_content">Content:</label>
					<input id='patch_content' type="text">
					<button id="patch_button">Change!</button>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
		<script src="/static/script.js"></script>
		</body>
		</html>
		<?php
	}

	/**
	 * Serve static files. Hate that I had to write this, NGINX/apache would
	 * normally do this for you but I'm just using the CLI web server for
	 * quickness' sake.
	 * @param $filename
	 * @return void
	 */
	static function serveStatic($filename) {
		error_log('static?!');
		if (preg_match('/css$/', $filename)) {
			header('Content-Type: text/css; charset=UTF-8');
		} elseif (preg_match('/css$/', $filename)) {
			header('Content-Type: script/javascript; charset=UTF-8');
		}
		$path = realpath(__DIR__ . "/../static/$filename");
		readfile($path);
	}

}